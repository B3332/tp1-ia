/* PageRank */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/* allocate one object of given type */
#define NEW(type) ((type*)calloc((size_t)1,(size_t)sizeof(type)))

/* allocate num objects of given type */
#define NEW_A(num,type) ((type*)calloc((size_t)(num),(size_t)sizeof(type)))

typedef unsigned int u_int;
typedef double Real;

/* vector definition */
typedef struct {
    u_int dim;
    Real *ve;
} VEC;

/* matrix definition */
typedef struct {
    u_int m, n;
    Real **me;
} MAT;

/* sparse matrix definition */
typedef struct graphnode {
    u_int col;
    Real val;
    struct graphnode *next;
} NODE;

typedef struct {
    u_int m, n;
    NODE *rows;
} SMAT;

/* v_get -- gets a VEC of dimension 'dim'
   Precondition: size >= 0
   Postcondition: initialized to zero */
VEC *v_get(u_int size) {
    VEC *v;

    if ((v = NEW(VEC)) == (VEC *) NULL) {
        fprintf(stderr, "v_get memory error");
        exit(-1);
    }

    v->dim = size;
    if ((v->ve = NEW_A(size, Real)) == (Real *) NULL) {
        free(v);
        fprintf(stderr, "v_get memory error");
        exit(-1);
    }

    return (v);
}

/* v_input -- file input of vector */
VEC *v_input(FILE *fp) {
    VEC *g;

    u_int m, n, val;

    /* get dimension */
    if (fscanf(fp, " Matrix: %u by %u", &m, &n) < 2) {
        fprintf(stderr, "v_input error reading dimensions");
        exit(-1);
    }

    /* allocate memory if necessary */
    g = v_get(m);

    /* get entries */
    Real init = 1.0 / m;
    for (u_int i = 0; i < m; i++) {
        g->ve[i] = init;
    }

    return (g);
}

/* v_input -- file input of vector */
VEC *v_input_mat(MAT *M) {
    VEC *g;

    u_int m, n, val;

    /* allocate memory if necessary */
    g = v_get(M->m);

    /* get entries */
    Real init = 1.0 / m;
    for (u_int i = 0; i < m; i++) {
        g->ve[i] = init;
    }

    return (g);
}

/* v_free -- returns VEC & associated memory back to memory heap */
int v_free(VEC *vec) {
    if (vec == (VEC *) NULL)
        return (-1);

    if (vec->ve == (Real *) NULL) {
        free(vec);
    } else {
        free(vec->ve);
        free(vec);
    }

    return (0);
}

SMAT *sm_get(u_int m, u_int n) {
    SMAT *G;

    if ((G = NEW(SMAT)) == (SMAT *) NULL) {
        fprintf(stderr, "sm_get memory error");
        exit(-1);
    }

    G->m = m;
    G->n = n;

    if ((G->rows = NEW_A(m, NODE)) == (NODE *) NULL) {
        free(G);
        fprintf(stderr, "sm_get memory error");
        exit(-1);
    }

    for (u_int i = 0; i < G->m; i++)
        (G->rows[i]).val = -1;

    return (G);
}

int sm_free(SMAT *G) {
    if (G == (SMAT *) NULL)
        return (-1);

    if (G->rows == (NODE *) NULL) {
        free(G);
    } else {
        NODE *n0;
        NODE *n1;
        for (u_int i = 0; i < G->m; i++) {
            n0 = &(G->rows[i]);
            if (n0->val < 0.0) break; /* empty line */
            n0 = n0->next;
            while (n0->val >= 0.0) {
                n1 = n0->next;
                free(n0);
                n0 = n1;
            }
            free(n0);
        }
        free(G->rows);
        free(G);
    }

    return (0);
}

NODE *sm_add(NODE *n0, u_int c, Real v) {
    NODE *n1;
    n0->col = c;
    n0->val = v;
    if ((n1 = NEW(NODE)) == (NODE *) NULL) {
        fprintf(stderr, "sm_add memory error");
        exit(-1);
    }
    n1->val = -1;
    n0->next = n1;
    return (n1);
}

/* m_get -- gets an mxn matrix by dynamic memory allocation 
   Precondition: m>=0 && n>=0
   Postcondition: initialized to zero */
MAT *m_get(u_int m, u_int n) {
    MAT *g;

    if ((g = NEW(MAT)) == (MAT *) NULL) {
        fprintf(stderr, "m_get memory error");
        exit(-1);
    }

    g->m = m;
    g->n = n;

    if ((g->me = NEW_A(m, Real*)) == (Real **) NULL) {
        free(g);
        fprintf(stderr, "m_get memory error");
        exit(-1);
    }

    for (int i = 0; i < m; i++)
        if ((g->me[i] = NEW_A(n, Real)) == (Real *) NULL) {
            fprintf(stderr, "m_get memory error");
            exit(-1);
        }

    return (g);
}

/* m_free -- returns MAT & associated memory back to memory heap */
int m_free(MAT *mat) {
    if (mat == (MAT *) NULL)
        return (-1);

    for (int i = 0; i < mat->m; i++)
        if (mat->me[i] != (Real *) NULL) free(mat->me[i]);

    if (mat->me != (Real **) NULL) free(mat->me);

    free(mat);

    return (0);
}

/* m_input -- file input of matrix */
MAT *m_input(FILE *fp) {
    MAT *g;
    u_int m, n, val;

    /* get dimension */
    if (fscanf(fp, " Matrix: %u by %u", &m, &n) < 2) {
        fprintf(stderr, "m_input error reading dimensions");
        exit(-1);
    }

    /* allocate memory if necessary */
    g = m_get(m, n);

    /* get entries */
    for (u_int i = 0; i < m; i++) {
        if (fscanf(fp, " row %u:", &val) < 1) {
            fprintf(stderr, "m_input error reading line %u", i);
            exit(-1);
        }
        for (u_int j = 0; j < n; j++)
            if (fscanf(fp, "%lf", &g->me[i][j]) < 1) {
                fprintf(stderr, "m_input error reading line %u col %u", i, j);
                exit(-1);
            }
    }

    return (g);
}

/* sm_input -- file input of sparse matrix */
SMAT *sm_input(FILE *fp) {
    SMAT *g;
    u_int m, n, row;
    Real col;
    NODE *n0;

    /* get dimension */
    if (fscanf(fp, " SparseMatrix: %u by %u", &m, &n) < 2) {
        fprintf(stderr, "sm_input error reading dimensions");
        exit(-1);
    }

    g = sm_get(m, n);

    /* get entries */
    for (u_int i = 0; i < m; i++) {
        if (fscanf(fp, " row %u:", &row) < 1) {
            fprintf(stderr, "sm_input error reading line %u", i);
            exit(-1);
        }
        n0 = &(g->rows[i]);
        for (;;) {
            if (fscanf(fp, "%lf", &col) < 1) {
                fprintf(stderr, "sm_input error reading line %u col x", i);
                exit(-1);
            }
            if (col < 0.0) break;
            n0 = sm_add(n0, (u_int) col, 1.0);
        }
    }

    return (g);
}

static char *format = "%1.5g ";

void sm_output(FILE *fp, SMAT *G) {
    NODE *n0;

    fprintf(fp, "SparseMatrix: %d by %d\n", G->m, G->n);
    for (u_int i = 0; i < G->m; i++) {
        fprintf(fp, "row %u: ", i);
        n0 = &(G->rows[i]);
        while (n0->val >= 0.0) {
            fprintf(fp, format, (Real) n0->col);
            n0 = n0->next;
        }
        fprintf(fp, "-1\n");
    }
}

/* m_output -- file output of matrix 
   Precondition: Memory already allocated for the matrix */
void m_output(FILE *fp, MAT *g) {
    u_int tmp;

    fprintf(fp, "Matrix: %d by %d\n", g->m, g->n);
    for (u_int i = 0; i < g->m; i++) {
        fprintf(fp, "row %u: ", i);
        tmp = 2;
        for (u_int j = 0; j < g->n; j++, tmp++) {
            fprintf(fp, format, g->me[i][j]);
            if (!(tmp % 9)) putc('\n', fp);
        }
        if (tmp % 9 != 1) putc('\n', fp);
    }
}

/* v_output -- file output of vector */
void v_output(FILE *fp, VEC *v) {
    fprintf(fp, "Vector: %d\n", v->dim);
    for (u_int i = 0; i < v->dim; i++) fprintf(fp, format, v->ve[i]);
    putc('\n', fp);
}

/* m_cp -- copy matrix M in OUT
   Precondition: memory is already allocated for M and OUT
   Precondition: sizes of M and OUT must match*/
MAT *m_cp(MAT *M, MAT *OUT) {
    for (u_int i = 0; i < M->m; i++)
        memmove(&(OUT->me[i][0]), &(M->me[i][0]), (M->n) * sizeof (Real));

    return (OUT);
}

/* v_cp -- copy vector v in out
   Precondition: memory is already allocated for v and out*/
VEC *v_cp(VEC *v, VEC *out) {
    memmove(&(out->ve[0]), &(v->ve[0]), (v->dim) * sizeof (Real));
    return (out);
}

//Création de H (pas bien)

/* m_power */ // NE MARCHE PAS BIEN ! (OU LONGUEMENT)

MAT* m_pow(MAT *G, u_int power) {

    //On créé la matrice résultat
    MAT *RES = m_get(G->m, G->n);
    m_cp(G, RES);

    u_int valTmp = 0;

    //Pour chaque power.
    for (u_int nbIt = 0; nbIt < power; power++) {
        // Pour chaque valeur de la matrice 
        for (u_int k = 0; k < G->m; k++) {
            for (u_int l = 0; l < G->n; l++) {

                //On parcours la matrice (longeur et largeur avec même indice)
                for (u_int i = 0; i < G->m; i++) {
                    valTmp = G->me[k][i] * G->me[i][l];
                }

                RES->me[k][l] = valTmp;
                valTmp = 0;
            }
        }
        m_cp(RES, G);
    }

    return RES;
}

MAT* h_creator(MAT* M) {
    MAT* H;
    //Récupère bonnes dimensions
    H = m_get(M->m, M->n);
    m_cp(M, H);

    u_int cardLigne = 0;
    for (u_int i = 0; i < H->m; i++) {
        // On compte
        for (u_int j = 0; j < H->n; j++) {
            cardLigne += H->me[i][j];
        }
        if (cardLigne != 0) {
            // On divise
            for (u_int j = 0; j < H->n; j++) {
                H->me[i][j] *= 1.0 / cardLigne;
            }
            cardLigne = 0;
        }
    }

    return H;
}

VEC* a_creator(MAT* M) {
    VEC* a;
    //Récupère bonnes dimensions
    a = v_get(M->m);

    u_int cardLigne = 0;
    for (u_int i = 0; i < M->m; i++) {
        // On compte
        for (u_int j = 0; j < M->n; j++) {
            cardLigne += M->me[i][j];
        }
        if (cardLigne != 0) {
            a->ve[i] = 0;
            cardLigne = 0;
        } else {
            //C'est un noeud absorbant
            a->ve[i] = 1;
        }
    }

    return a;
}

//Création de H stochastique

MAT* h_sto_creator(MAT* M) {
    MAT* H;
    //Récupère bonnes dimensions
    H = m_get(M->m, M->n);
    m_cp(M, H);

    u_int cardLigne = 0;
    for (u_int i = 0; i < H->m; i++) {
        // On compte
        for (u_int j = 0; j < H->n; j++) {
            cardLigne += H->me[i][j];
        }
        if (cardLigne != 0) {
            // On divise
            for (u_int j = 0; j < H->n; j++) {
                H->me[i][j] *= 1.0 / cardLigne;
            }
            cardLigne = 0;
        } else {
            /* SOLUTION 1 : lien vers elle même */
            //pas vraiment bonne en fait. =)

            /* SOLUTION 2 : égaliser la ligne */
            for (u_int j = 0; j < H->n; j++) {
                H->me[i][j] = 1.0 / H->n;
            }
            //*/
        }
    }

    return H;
}

//Création de H ergodique

MAT* h_ergo_creator(MAT* M, Real alpha) {

    //Récupère bonnes dimensions et la transforme en stochastique
    MAT* H;
    H = m_get(M->m, M->n);
    H = h_sto_creator(M);

    //Créer la matrice de rang 1 à partir de vecteur unitaire
    MAT* matRangUn;
    matRangUn = m_get(M->m, M->n);

    //Créer la matrice de sortie
    MAT* E;
    E = m_get(M->m, M->n);

    for (u_int i = 0; i < H->m; i++) {
        for (u_int j = 0; j < H->n; j++) {
            matRangUn->me[i][j] = 1;
            E->me[i][j] = alpha * H->me[i][j] + (1.0 - alpha)*(1.0 / H->m) * matRangUn->me[i][j];
        }
    }

    return E;
}

MAT* convertSMATtoMAT(SMAT* H) {

    //On initialise notre matrice finale
    MAT* result = m_get(H->m, H->n);

    for (u_int i = 0; i < H->m; i++) {
        for (u_int j = 0; j < H->n; j++) {
            if (H->rows[i].col == j) {
                result->me[i][j] = H->rows[i].val;
            } else {
                result->me[i][j] = 0;
            }
        }
    }

    return result;
}

VEC* algo(MAT* H, VEC* R_zero, u_int nbIt) {

    //On initialise notre R "final"
    VEC* R = v_get(R_zero->dim);
    v_cp(R_zero, R);
    //On initialise un R temporaire pour ne pas perturber le calcul
    VEC* Rtmp = v_get(R->dim);
    v_cp(R, Rtmp);

    Real resultTMP = 0;

    if (R->dim == H->m && H->m == H->n) {

        //Boucle sur nombre d'itérations
        for (u_int k = 0; k < nbIt; k++) {

            // Pour chaque case du vecteur de Rn+1
            for (u_int i = 0; i < H->n; i++) {
                //Pour chaque case du vecteur Rn
                for (u_int j = 0; j < R->dim; j++) {
                    // On range dans un résultat temporaire la multiplication d'une case de Rn avec un chiffre de la colonne de la matrice
                    resultTMP += R->ve[j] * H->me[j][i];
                }
                //Une fois le résultat TMP calculé, on le range dans RN+1
                Rtmp->ve[i] = resultTMP;
                //On remet le résultat TMP à zéro pour la prochaine itération
                resultTMP = 0;
            }

            //On range RN+1 dans RN
            v_cp(Rtmp, R);
        }
    } else {
        printf("Matrice pas carré ! ");
    }

    // DEBUG = AFFICHAGE SOMME
    Real somme = 0;
    for (u_int i = 0; i < R->dim; i++) {
        somme += R->ve[i];
    }
    printf("SOMME: %f\n", somme);
    // FIN DEBUG

    return R;
}

VEC* algoPi(MAT* H, VEC* R_zero, VEC* a, Real alpha, u_int nbIt) {

    //On initialise notre R "final"
    VEC* R = v_get(R_zero->dim);
    v_cp(R_zero, R);
    //On initialise un R temporaire pour ne pas perturber le calcul
    VEC* Rtmp = v_get(R->dim);
    v_cp(R, Rtmp);

    Real resultTMP = 0;
    Real resultTMPra = 0;

    if (R->dim == H->m && H->m == H->n) {

        //Boucle sur nombre d'itérations
        for (u_int k = 0; k < nbIt; k++) {

            // Pour chaque case du vecteur de Rn+1
            for (u_int i = 0; i < H->n; i++) {
                //Pour chaque case du vecteur Rn
                for (u_int j = 0; j < R->dim; j++) {
                    // On range dans un résultat temporaire la multiplication d'une case de Rn avec un chiffre de la colonne de la matrice
                    resultTMP += R->ve[j] * H->me[j][i];
                    // Calcul de la deuxième multiplication : R(k)*a
                    resultTMPra += R->ve[j] * a->ve[j];
                }
                //Une fois le résultat TMP calculé, on le range dans RN+1
                Rtmp->ve[i] = alpha * resultTMP + (alpha * resultTMPra + 1 - alpha)*(1.0 / H->m);
                //On remet le résultat TMP à zéro pour la prochaine itération
                resultTMP = 0;
                resultTMPra = 0;
            }

            //On range RN+1 dans RN
            v_cp(Rtmp, R);
        }
    } else {
        printf("Matrice pas carré ! ");
    }

    // DEBUG = AFFICHAGE SOMME
    Real somme = 0;
    for (u_int i = 0; i < R->dim; i++) {
        somme += R->ve[i];
    }
    printf("SOMME: %f\n", somme);
    // FIN DEBUG

    return R;
}

VEC* algoSMAT(SMAT* H,  Real alpha, u_int nbIt) {

    VEC* a = v_get(H->m); //m = nb de lignes
    
    //On initialise notre R "final"
    VEC* R = v_get(H->m);
    Real init = 1.0 / H->m;
    for (u_int i = 0; i < m; i++) {
        R->ve[i] = init;
    }

    //On initialise un R temporaire pour ne pas perturber le calcul
    VEC* Rtmp = v_get(R->dim);
    v_cp(R, Rtmp);

    Real resultTMP = 0;
    Real resultTMPra = 0;

    for(u_int i = 0 ; i < H->m ; i++){
        // On crible A
        if(H->rows[i]==null){
            a[i] = 1;
        } else {
            a[i] = 0;
        }
        
        //On fait les noeuds
        Rtmp[H->rows[i].col] = alpha*R[H->rows.col]+(alpha*R[i]*a)
    }
    
    
    // TO DO 
    
    
    
    return R;
}


void main() {

    //Test créer vecteur
    VEC *v;
    v = v_get(3);
    printf("size: %d\n", v->dim);
    v_free(v);

    //Test créer matrice
    MAT *G;
    G = m_get(3, 3);
    printf("nb-line: %d ; nb-col: %d\n", G->m, G->n);
    m_free(G);

    //Test récupération de matrice à partir fichier
    FILE *fp;
    fp = fopen("dataset/g.dat", "r");
    G = m_input(fp);
    fclose(fp);
    m_output(stdout, G);
    m_free(G);

    // ================== Test de l'algorithme sur petit jeu de test ==================

    // ====== Valeurs nécessaires ======
    MAT *H;
    VEC *aVecNoeudsAbsorbants;
    Real alpha = 0.9;
    //On créé nos vecteurs de liens initiaux et finaux
    VEC *rZero;
    VEC *rIterationN;
    VEC *rIterationNGenetic;

    //On ouvre le fichier de base
    fp = fopen("dataset/g.dat", "r");

    // ====== Créations des matrices ======

    //Récupère la matrice M initiale
    G = m_input(fp);
    printf("G:\n");
    m_output(stdout, G);

    //On créé H à partir de M
    // Mauvais créator, pas stochastique // H = h_creator(G);
    // Juste stochastique, sans surfeur aléatoire // H = h_sto_creator(G); 
    H = h_ergo_creator(G, alpha);
    printf("H:\n");
    m_output(stdout, H);


    //Réouverture du fichier
    fclose(fp);
    fp = fopen("dataset/g.dat", "r");

    //On créé le R initial à partir du fichier
    rZero = v_input(fp);
    printf("R N°0:\n");
    v_output(stdout, rZero);

    // On créer la matrice de noeuds absorbants
    aVecNoeudsAbsorbants = a_creator(G);
    printf("aVecNoeudsAbsorbants:\n");
    v_output(stdout, aVecNoeudsAbsorbants);

    //Fermeture du fichier
    fclose(fp);

    // ====== Algos ======

    //On fait tourner l'algo N°1
    rIterationN = algo(H, rZero, 150);

    printf("R N°n:\n");
    v_output(stdout, rIterationN);

    //On fait tourner l'algo sur une matrice creuse
    H = h_creator(G);
    rIterationNGenetic = algoPi(H, rZero, aVecNoeudsAbsorbants, alpha, 150);

    printf("R N°n Genetic:\n");
    v_output(stdout, rIterationNGenetic);

    // ====== Autres calculs ======

    //On calcul des puissances
    //    MAT* resPow = m_pow(G, 2); 
    //    printf("resPow:\n");
    //    m_output(stdout, resPow);

    // ====== Nettoyage ======

    m_free(G);
    m_free(H);
    v_free(rZero);

    // ================== Test de l'algorithme sur genetic.dat ==================

    SMAT *SG;
    fp = fopen("dataset/genetic.dat", "r");
    SG = sm_input(fp);
    printf("2> Parsing du SG\n");
    //Fermeture du fichier
    fclose(fp);

    // ====== Créations des matrices ======
    //Récupère la matrice M initiale
    MAT* convertedSG = convertSMATtoMAT(SG);
    printf("Matrice convertie\n");

    //Reallocaiton
    G = m_get(convertedSG->m, convertedSG->n);
    m_cp(convertedSG, G);
    printf("2> G:\n");
    //m_output(stdout, G);

    //On créé H à partir de M
    H = h_creator(G);
    printf("2> H:\n");
    //m_output(stdout, H);

    //On créé le R initial à partir du fichier
    rZero = v_input_mat(G);
    printf("2> R N°0:\n");
    //v_output(stdout, rZero);

    // On créer la matrice de noeuds absorbants
    aVecNoeudsAbsorbants = a_creator(G);
    printf("2> aVecNoeudsAbsorbants:\n");
    //v_output(stdout, aVecNoeudsAbsorbants);

    // ====== Algos ======
    //On fait tourner l'algo sur une matrice creuse
    rIterationNGenetic = algoPi(H, rZero, aVecNoeudsAbsorbants, alpha, 150);
    printf("2> R N°n Genetic:\n");
    v_output(stdout, rIterationNGenetic);

    // ====== Nettoyage ======
    sm_free(SG);

    // ================== Dernier : test.dat ==================
    fp = fopen("dataset/test.dat", "w");
    //sm_output(fp, SG);
    //Fermeture du fichier
    fclose(fp);

    //On ouvre le fichier de base
    fp = fopen("dataset/test.dat", "r");

    exit(0);
}
